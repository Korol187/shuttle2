const mongoose = require('mongoose');

const Schema = mongoose.Schema;

module.exports = new Schema({
    IsActive: {
        type: Boolean,
        default: true,
    },
    CreateDate: {
        type: Date,
        default: Date.now,
    },
    UpdateDate: {
        type: Date,
        required: false,
    },
},
{ versionKey: false });
