const mongoose = require('mongoose');
require('mongoose-schema-extend');
const SchemaBase = require('./base/base.model');
const modelNames = require('../constants/model.names');
const Promise = require('bluebird');

mongoose.Promise = Promise;

const newsSchema = SchemaBase.extend({
    Description: {
        type: String,
        required: true,
    },
    Image: {
        type: String,
        required: false,
    },
    Title: {
        type: String,
        required: true,
    },
    CategoryId: {
        type: Number,
        required: true,
    },
});

const News = mongoose.model(modelNames.news, newsSchema);

module.exports = News;
