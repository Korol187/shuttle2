module.exports = {
    rootEndpoint: '/',
    getByIdEndpoint: '/:id',
    pingEndpoint: '/ping',
    petsEndpoint: '/pets',
    donationEndpoint: '/donation',
    newsEndpoint: '/news',
    userEndpoint: '/user',
    badEndpoint: '/badlogin',
};
