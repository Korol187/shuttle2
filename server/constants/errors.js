module.exports = {
    NOT_FOUND_EXCEPTION_MESSAGE: 'Not found exception',
    INTERNAL_ERROR_MESSAGE: 'Unhandled Exception',
    CONFIG_NOT_FOUND_MESSAGE: 'Config was not found',
    BAD_REQUEST_MESSAGE: 'Bad request',
};
