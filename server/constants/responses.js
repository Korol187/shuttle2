const statusCodes = require('http-status-codes');

module.exports = {
    ok: message => {
        const status = statusCodes.OK;
        return { status, message };
    },
};
