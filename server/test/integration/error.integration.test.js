const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../app');
const httpStatusCodes = require('http-status-codes');
const expect = chai.expect; //eslint-disable-line
const should = chai.should(); //eslint-disable-line

chai.use(chaiHttp);
const errorRootRoute = '/badroute';
const errorTestRout = '/badlogin';


describe('Errors API tests', () => {
    it('Get response for bad route', (done) => {
        chai.request(server)
        .get(errorRootRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.NOT_FOUND);
            done();
        });
    });

    it('Get response for 403 error', (done) => {
        chai.request(server)
        .get(errorTestRout)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.FORBIDDEN);
            done();
        });
    });
});
