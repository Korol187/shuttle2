const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../app');
const httpStatusCodes = require('http-status-codes');
const expect = chai.expect; //eslint-disable-line
const should = chai.should(); //eslint-disable-line

chai.use(chaiHttp);
const newsRootRoute = '/news';
const existingNewsRoute = '/news/1';

describe.skip('News API test', () => {
    it('Get all news', (done) => {
        chai.request(server)
        .get(newsRootRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });

    it('Get existing news record by Id', (done) => {
        chai.request(server)
        .get(existingNewsRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });

    it('Create new news record', (done) => {
        chai.request(server)
        .post(newsRootRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });

    it('Update existing news record by Id', (done) => {
        chai.request(server)
        .put(existingNewsRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });

    it('Delete existing news record by Id', (done) => {
        chai.request(server)
        .delete(existingNewsRoute)
        .end((err, res) => {
            res.should.have.status(httpStatusCodes.OK);
            done();
        });
    });
});
