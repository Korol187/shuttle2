const mongoose = require('mongoose');
const errors = require('../../constants/errors');
const ErrorBase = require('../../errors/error-base');
const httpStatusCode = require('http-status-codes');

class ConnectionProvider {
    constructor(config) {
        this.config = config;
        this.mongoose = null;
    }

    openConnection() {
        if (this.config) {
            this.mongoose = mongoose.connect(`mongodb://${this.config.host}:${this.config.port}/${this.config.dbName}`);
        } else {
            throw new ErrorBase(errors.CONFIG_NOT_FOUND_MESSAGE, httpStatusCode.NOT_FOUND);
        }

        return this.mongoose.connection;
    }

    closeConnection() {
        if (this.mongoose.connection) {
            this.mongoose.connection.close();
        }
    }
}

module.exports = ConnectionProvider;
