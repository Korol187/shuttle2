const User = require('../models/user.model');
const BaseProvider = require('./base/data.base.service');

class UserProvider extends BaseProvider {
}

module.exports = new UserProvider(User, {});
