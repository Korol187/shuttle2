const app = require('../app');
const http = require('http');
const serverUtility = require('../utility/server.utility');
const debug = require('debug')('findthemhome:server');
const config = require('../config/mongo.config.js');
let Provider = null;

const port = serverUtility.normalizePort(process.env.PORT || '3000');
app.set('port', port);

const server = http.createServer(app);

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    if (Provider) {
        Provider.closeConnection();
    }

    const bind = typeof port === 'string'
      ? `Pipe ${port}`
      : `Port ${port}`;

    switch (error.code) {
    case 'EACCES':
        console.error(`${bind} requires elevated privileges`); //eslint-disable-line
        process.exit(1);
        break;
    case 'EADDRINUSE':
        console.error(`${bind} is already in use`); //eslint-disable-line
        process.exit(1);
        break;
    default:
        throw error;
    }
}

function onListening() {
    Provider = new (require('../services/base/connection.service'))(config); //eslint-disable-line
    Provider.openConnection();
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? `pipe ${addr}`
        : `port ${addr.port}`;
    debug(`Listening on ${bind}`);
}

function onClose() {
    Provider.closeConnection();
}

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);
server.on('close', onClose);
