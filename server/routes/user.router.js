const express = require('express');
const router = express.Router(); //eslint-disable-line

const userService = require('../services/user.service');
const RouterProvider = require('./base/base.router');

const userProvider = new RouterProvider(userService, router);
module.exports = userProvider.router;
