# Project Shuttle 2

Our client is [Animal Rescue](http://spt-kh.com/). We are creating Mobile app for Google play and App store. 
That is volunteer's project where our purposes:

1. Create a product which can help to save homeless/disabled animals;
2. Create 3 categories which include: 
    * homeless dogs look for home;
    * homeless cats look for home;
    * disabled animals look for home;
    * lost pedigree animals;
    
* 0.0.1

### Setup ###

* Project donwload we use next command:

```bash
git clone (https://bitbucket.org/lototskiy/shuttle2.git)
cd server
```

* Configuration:
    *comming soon;
* Dependencies:


1. Front End:  
    * Cordova;
    * React.js;
    * Redux and Flux;
    * React - Router;
    * ES7/ES next;
    * Babel.js
    * Unit test - Karma(test runner)/Mocha+Chai+Enzyme;
    * ESLint;
    * Webpack;


2. Back end:
    * Node.js
    * Express.js;
    * MongoDB;
    * Bluebird;
    * ES6;
    * Mongoose.js;
    * npm>3;
    * Bottle.js;




* Database configuration 
    * comming soon;  


* Environment Setup
    * NodeJS version 7.1.0 (or 6.9.1 if you have problems with tests running)
    * NPM version 3.0+ (included with node)

* How I can use Mocha and Chai for testing? Usage

```bash
  npm install
```  
  must run from root server folder (where the package.json file is located)

```bash 
npm install mocha --save
```
install mocha

```bash
npm install chai --save
```
install chai

```bash
npm test
```    
run all tests located under the test folder (e.g. the 'server' folder)

```bash 
mocha filename.js
``` 
run the exact test, must run from folder where test is

* Deployment instructions
    * coming soon;

### Contribution guidelines ###

* Writing tests
    * coming soon;
* Code review
    * coming soon;
* Other guidelines
    * coming soon;

### Contacts ###

* [Nickolay Lototskiy](https://bitbucket.org/lototskiy/);
* [Skype](maltez66) Back End Tech Lead;
* [Skype](zenway.js) Front End Tech Lead;
* [Skype](d.syrkin) Project Manager & Test Lead;
* Our [community](http://it-shuttle.com/);
* For any additional information, please, contact [us](info@it-shuttle.com);


 
 
 
 